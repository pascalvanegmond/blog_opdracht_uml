<?php
require_once('classes/User.php');
require_once('classes/Blogpost.php');
require_once('classes/Comment.php');

echo 'User (create)<pre>';
$user = new User;
$user->create($username ='pascal', $email = 'test@test.nl', $password = 'hoi');
var_dump($user);
echo '</pre>';

echo ' User (update and delete)<pre>';
$user->update($user, $username = null, $email = "newemail@mail.nl");
$user->delete();
var_dump($user);
echo '</pre>';

echo 'Blogpost (create a blogpost)<pre>';
$blogpost = new Blogpost;
$blogpost->create($user, 'first blog', 'lorem ipsum', 'today');
var_dump($blogpost);
echo '</pre>';

echo 'Blogpost (create, set user to author role)<pre>';
$user->setRole('author');
$blogpost->create($user, 'first blog', 'lorem ipsum', 'today');
var_dump($blogpost);
echo '</pre>';

echo 'Blogpost (update, delete)<pre>';
$blogpost->update($blogpost, $user, 'first edit', null, 'yesterday');
$blogpost->delete($user);
var_dump($blogpost);
echo '</pre>';


echo 'Blogpost (update, delete and set user to admin)<pre>';
$user->setRole('admin');
$blogpost->update($blogpost, $user, 'first edit', null, 'yesterday');
$blogpost->delete($user);
var_dump($blogpost);
echo '</pre>';

echo 'Blogpost (share and show)<pre>';
$blogpost->share($user, $blogpost);
echo '</pre>';

echo 'Comment (create)<pre>';
$comment = new Comment;
$comment->create($user, $blogpost, 'Cool post! keep up the work!', 'today');
var_dump($comment);
echo '</pre>';

echo 'Comment (update, delete)<pre>';
$comment->update($comment, 'Cool post! keep up the work! <i>edited</i>', 'today');
$comment->delete();
var_dump($comment);
echo '</pre>';

echo 'Comment (show)<pre>';
$comment->show($comment);
echo '</pre>';

?>