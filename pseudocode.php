<?php

Class User {

	public $username;
	public $email;
	public $password = 'geheim';
	public $role = 'user';
	private $_deleted = false;

	public function create($username, $email, $password){
		// create a new user.
	}

	public function update(User $user, $username, $email, $password){
		// update the user.
	}

	public function delete(){
		// delete the user.
	}

	public function setRole($role){
		// upgrade or downgrade the user to user, author or admin.
	}

}

Class Blogpost {

	public $title;
	public $body;
	public $publish_date;
	public $author;
	private $_deleted = false;

	public function show(Blogpost $blogpost){
		// show the post.
	}

	public function create(User $user, $title, $body, $publish_date){
		// create a new post if the user is an author or admin if the user is and author or an admin.
	}

	public function update(Blogpost $blogpost, User $user, $title, $body, $publish_date, $author = null){
		// update the blogpost if the user is an admin.
	}

	public function delete(User $user){
		// set $_deleted to false if the user is an admin.
	}

	public function share(User $user, Blogpost $blogpost){
		// a user shares a blogpost..
	}
}

Class Comment {

	public $author;
	public $blogpost;
	public $message;
	public $publish_date;
	private $_deleted = false;

	public function show(Comment $comment){
		// show the comment.
	}

	public function create(User $author, Blogpost $blogpost, $message, $publish_date){
		// create a new comment.
	}

	public function update(Comment $comment, $message){
		// update the comment
	}

	public function delete(){
		// set $_deleted to true.
	}

}

?>
