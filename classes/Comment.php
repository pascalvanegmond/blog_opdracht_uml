<?php

Class Comment {
	public $author;
	public $blogpost;
	public $message;
	public $publish_date;
	private $_deleted = false;

	public function show(Comment $comment){

		// show the object comment.
		print_r($comment);

	}

	public function create(User $author, Blogpost $blogpost, $message, $publish_date){

		// create a new object 'comment'
		$this->author = $author->username;
		$this->blogpost = $blogpost->title;
		$this->message = $message;
		$this->publish_date = $publish_date;

	}

	public function update(Comment $comment, $message){

		// you can only update your message.
		if($message != null){
			$this->message = $message;
		}
		else{
			$this->message = $comment->message;
		}

	}

	public function delete(){

		// delete the message.
		$this->_deleted =  true;

	}

}

?>
