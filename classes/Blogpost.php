<?php

Class Blogpost {
	public $title;
	public $body;
	public $publish_date;
	public $author;
	private $_deleted = false;

	public function show(Blogpost $blogpost){

		// show the object blogpost.	
		print_r($blogpost);

	}


	public function create(User $user, $title, $body, $publish_date){

		// create a new blogpost. and checks if you are an auhtor or and admin.
		if ($user->role == 'admin' or $user->role == 'author'){
			$this->title = $title;
			$this->body = $body;
			$this->publish_date = $publish_date;
			$this->author = $user->username;

		}else {
			echo '<p>Sorry your are not alloud to post! </p>';
		}

	}


	public function update(Blogpost $blogpost, User $user, $title, $body, $publish_date, $author = null){

		// check if the user is an admin. and if the var's are not null. if they are null they get the old property of the object.
		if ($user->role == 'admin'){

			if($title != null){
				$this->title = $title;
			}
			else{
				$this->title = $blogpost->title;
			}

			if($body != null){
				$this->body = $body;
			}
			else{
				$this->body = $blogpost->body;
			}

			if($publish_date != null){
				$this->publish_date = $publish_date;
			}
			else{
				$this->publish_date = $blogpost->publish_date;
			}

			if($author != null){
				$this->author = $author;
			}
			else{
				$this->author = $user->username;
			}

		}else {
			echo '<p>Sorry your are not alloud to update this post! </p>';
		}
	}


	public function delete(User $user){

		// check if the user is an admin if its true, delete the object blogpost.
		if ($user->role == 'admin'){
			$this->_deleted = true;
		}else {
			echo '<p>Sorry your are not alloud to delete this post! </p>';
		}
	}


	public function share(User $user, Blogpost $blogpost){

		// print the string with username and print_r the object.
		echo '<p>' . $user->username . ' would like to share you this blogpost</p>';
		print_r($blogpost);

	}

}

?>
