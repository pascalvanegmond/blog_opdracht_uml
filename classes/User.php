<?php

Class User {
	public $username;
	public $email;
	public $password = 'geheim';
	public $role = 'user';
	private $_deleted = false;

	public function create($username, $email, $password = null){

		// create a new user.
		$this->username = $username;
		$this->email = $email;
		if($password != null){
			$this->password = $password;
		}
	}

	public function update(User $user, $username = null, $email = null, $password = null){

		// update the object user. And the variable's are are null they get the old property of the object.
		if($username != null){
			$this->username = $username;
		}
		else{
			$this->username = $user->username;
		}

		if($email != null){
			$this->email = $email;
		}
		else{
			$this->email = $user->email;
		}

		if($password != null){
			$this->password = $password;
		}
		else{
			$this->password = $user->password;
		}
		
	}

	public function delete(){

		// delete the user.
		$this->_deleted =  true;

	}


	public function setRole($role){

		// change the role of an user.
		if($role == 'admin'){
			$this->role = 'admin';
		}

		if($role == 'author'){
			$this->role = 'author';
		}

		if($role == 'user'){
			$this->role = 'user';
		}

	}

}

?>
